variable "container_name" {
  description = "The sonar container name"
  default = "sonarqube_container"
  type = string
}
variable "image" {
  description = "The sonarqube image"
  default = "sonarqube"
  type = string
}
variable "network_name" {
  description = "The network name"
  default = "tf-network"
  type = string
}
variable "virtual_host" {
  description = ""
  default = "sonar.allence.cloud"
}
