terraform {
  required_providers {
    docker = {
      source = "terraform-providers/docker"
    }
    
  }
  
}

resource "docker_image" "sonar" {
  name = var.image

}


resource "docker_container" "sonar-container" {
  image = docker_image.sonar.name
  name  = var.container_name

  networks_advanced {
    name = var.network_name
  }
  env = [
    "VIRTUAL_HOST=${var.virtual_host}",
    "VIRTUAL_PORT=9000",
    "VIRTUAL_PROTO=http",
    "CERT_NAME=${var.virtual_host}",
    "LETSENCRYPT_HOST=${var.virtual_host}",
  ]
  
}



